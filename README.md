# Simple Auth
Simple authorization demo. Can be used on single page applications with no backend required. Shouldn't never be used on production.
## Setup
``` npm install ```
## Building
``` node build.js ```
## Deploying
``` wt create --secret mongoUrl={your_mongodb_url} dist.js ```
## Usage
``` https://wt-app-url.com/?method={method_name}&{additinal arguments} ```
## Avaivable methods
### SignUp
login - desired user login

password - desired user password

Example:
``` https://wt-app-url.com/?method=signUp&login=test1&password=test123 ```

Response:
```json
{
  "user": {
    "_id": "56c602217327d70100905ecf",
    "login": "test1",
    "token": "5e908cbc262832010ba9eb52fe593703c3af207d2ad6643c9b0b8015dfda919d84dd147835e2c0a94bf4be3cc902ef17"
  }
}
```
### SignIn
login - user defined login

password - user defined password

Example:
``` https://wt-app-url.com/?method=signIn&login=test1&password=test123 ```

Response:
```json
{
  "user": {
    "_id": "56c602217327d70100905ecf",
    "login": "test1",
    "token": "5e908cbc262832010ba9eb52fe593703c3af207d2ad6643c9b0b8015dfda919d84dd147835e2c0a94bf4be3cc902ef17"
  }
}
```
### Session
token - previously given token

password - user defined password

Example:
``` https://wt-app-url.com/?method=session&token=5e908cbc262832010ba9eb52fe593703c3af207d2ad6643c9b0b8015dfda919d84dd147835e2c0a94bf4be3cc902ef17 ```

Response:
```json
{
  "user": {
    "_id": "56c602217327d70100905ecf",
    "login": "test1",
    "token": "5e908cbc262832010ba9eb52fe593703c3af207d2ad6643c9b0b8015dfda919d84dd147835e2c0a94bf4be3cc902ef17"
  }
}
```

## Demo
``` https://webtask.it.auth0.com/api/run/wt-mikrowelt-gmail_com-0/dist?webtask_no_cache=1 ```
