'use strict';

let webpack = require("webpack");

let compiler = webpack({
	entry: './index.js',
	output: {
			path: __dirname,
			filename: 'dist.js',
			libraryTarget: 'commonjs2'
	},
	target: 'node',
	externals: [
		{
			q: true,
			mongodb: true,
			util: true,
			crypto: true
		}
	]
});

compiler.run(function (err) {
	if (err) {
		console.log('Error:', err);
	} else {
		console.log('Build successful');
	}
});
