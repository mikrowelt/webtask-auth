var Db = require('./db');
var controllers = {
  signIn: require('./controllers/signin'),
  signUp: require('./controllers/signup'),
  session: require('./controllers/session')
};

module.exports = function (ctx, cb) {
  var params = ctx.data;
  if (controllers[params.method]) {
    var db = new Db(params.mongoUrl);
    new controllers[params.method](params, cb, db);
  } else {
    cb('Invalid method name');
  }

};
