function Controller(params, response, db) {
  this.params = params;
  this.db = db;
  this.response = response;
  this.requestHandler();
}

Controller.prototype.requestHandler = function () {
  this.response(null, 'What\'s Up?');
};

Controller.prototype.responseHandler = function () {

};

module.exports = Controller;
