var Controller = require('./controller');
var util = require('util');

function SessionController() {
  Controller.apply(this, arguments);
}

util.inherits(SessionController, Controller);

SessionController.prototype.requestHandler = function () {
  var token = this.params.token;
  if (token) {
    var response = {};
    this.db.connect()
    .then(function() {
      return this.db.getUser({token: token});
    }.bind(this))
    .then(function(user) {
      if (!user) {
        this.response('Not authorized!');
      } else {
        delete user.password;
        response.user = user;
      }
    }.bind(this), function(err) {
      this.db.close();
      this.response(err);
    }.bind(this));
  } else {
    this.response('not enough arguments');
  }
};

module.exports = SessionController;
