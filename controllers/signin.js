var Controller = require('./controller');
var util = require('util');

function SignInController() {
  Controller.apply(this, arguments);
}

util.inherits(SignInController, Controller);

SignInController.prototype.requestHandler = function () {
  var login = this.params.login;
  var password = this.params.password;
  if (login && password) {
    var response = {};
    this.db.connect()
    .then(function() {
      return this.db.getUser({login: login});
    }.bind(this))
    .then(function(user) {
      if (!user || user.password !== password) {
        throw new Error('Username/Password doesnt match');
      }
      delete user.password;
      response.user = user;
      return this.db.loginUser(user._id);
    }.bind(this))
    .then(function (data) {
      response.user.token = data.token;
      this.response(null, response);
    }.bind(this), function(err) {
      this.db.close();
      this.response(err);
    }.bind(this));
  } else {
    this.response('not enough arguments');
  }
};

module.exports = SignInController;
