var Controller = require('./controller');
var util = require('util');

function SignUpController() {
  Controller.apply(this, arguments);
}

util.inherits(SignUpController, Controller);

SignUpController.prototype.requestHandler = function () {
  var login = this.params.login;
  var password = this.params.password;
  if (login && password) {
    var response = {};
    this.db.connect()
    .then(function() {
      return this.db.createUser(login, password);
    }.bind(this))
    .then(function(user) {
      delete user.password;
      response.user = user;
      return this.db.loginUser(user._id);
    }.bind(this))
    .then(function (data) {
      response.user.token = data.token;
      this.response(null, response);
    }.bind(this), function(err) {
      this.db.close();
      this.response(err);
    }.bind(this));
  } else {
    this.response('not enough arguments');
  }
};

module.exports = SignUpController;
