var q = require('q');
var MongoClient = require('mongodb').MongoClient;
var token = require('./utils/token');

function DataLayer(mongoUrl) {
  console.log(mongoUrl);
  this.mongoUrl = mongoUrl;
}

DataLayer.prototype.connect = function () {
  var result = q.defer();
  MongoClient.connect(this.mongoUrl, function(err, db) {
    if (err) {
      result.reject(err);
    } else {
      this.db = db;
      this.accounts = this.db.collection('accounts');
      result.resolve(db);
    }
  }.bind(this));
  return result.promise;
};

DataLayer.prototype.close = function () {
  this.db.close();
};

DataLayer.prototype.getUser = function (query) {
  var result = q.defer();
  this.accounts.findOne(query, function (err, data) {
    console.log('getUser', err, data);
    if (err) {
      result.reject(err);
    } else {
      result.resolve(data);
    }
  });
  return result.promise;
};

DataLayer.prototype.createUser = function (loginName, password) {
  var result = q.defer();
  this.getUser({login: loginName})
  .then(function(data) {
    if (data) {
      result.reject('user already exists');
    } else {
      this.accounts
      .insert({login: loginName, password: password}, function (err, data) {
        if (err) {
          result.reject(err);
        } else {
          var userData;
          if (data && data.ops && data.ops[0]) {
            userData = data.ops[0];
          }
          result.resolve(userData);
        }
      });
    }
  }.bind(this), result.reject.bind(result));
  return result.promise;
};

DataLayer.prototype.loginUser = function (userId) {
  var result = q.defer();
  var sessionToken = token();
  this.accounts
  .update({_id: userId}, {$set: {token: sessionToken}}, function (err, data) {
    if (err) {
      result.reject(err);
    } else {
      result.resolve({token: sessionToken});
    }
  });
  return result.promise;
};

module.exports = DataLayer;
